export interface User {
	username: string;
	roles: string[];
	email: string;
	name: string;
}
