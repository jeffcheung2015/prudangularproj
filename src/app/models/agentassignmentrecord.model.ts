export class AgentAssignmentRecord {
	prodClass : string;
	polNo : string;
	lifePolNo : string;
	applicantName : string;

  idNo : string;
	giOptOut : string;
	lifeOptOut : string;
	currentAgentName : string;

  currentAgentCode : string;
	currentAgentPhone : string;
	dateOfSubmission : string;

  campaignCode : string;
	assignmentType : string;
	agentTeam : string;

  agentCode : string;
	agentName : string;
	agentPhone : string;

  agentAssignedDate : string;
	agentSentDate : string;
	customerSentDate : string;
	lastEmailId: string;
}
