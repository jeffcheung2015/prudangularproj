export interface Pddcampaign {
campaignCode: string;
partnerCode: string;
partnerName: string;
startDt: Date;
endDt: Date;
giProductCode: string;
giProductSubtype: string;
premiumForAm: number;
amEntitled: number;
premiumDisc: number;
remarks: string;
genericPromoCode: string;
uniquePromoCodePool: string;
uniquePromoCodeQty: number;
promoCodeUsageUrl: string;
lastUpdUsr: string;
lastUpdDt: Date;
}