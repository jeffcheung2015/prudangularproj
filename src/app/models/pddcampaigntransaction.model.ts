export interface Pddcampaigntransaction {
	polNo: string;
	campaignCode: string;
	partnerCode: string;
	premDisc: number;
	mileage: number;
	status: string;
	createUsr: string;
	createDt: string;
	lastUpdUsr: string;
	lastUpdDt: string;
}
